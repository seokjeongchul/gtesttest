// my_math_test.c
#include <stdio.h>
#include <gtest/gtest.h>
#include "my_math.h"

TEST(Math_Test, checkPrimeNumber) {
    ASSERT_TRUE(1 == checkPrimeNumber(3));
}

TEST(Math_Test, checkPrimeNumber) { ASSERT_EQ(1, checkPrimeNumber(3)); }

TEST(Math_Test, checkPrimeNumber2) { ASSERT_EQ(0, checkPrimeNumber(8)); }

TEST(Math_Test, checkPrimeNumber3) { ASSERT_EQ(0, checkPrimeNumber(4)); }

TEST(Math_Test, checkPrimeNumber4) { ASSERT_EQ(1, checkPrimeNumber(7)); }